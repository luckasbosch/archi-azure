#!/bin/bash
sudo apt update
sudo apt -y upgrade

# Postgre SQL Installation
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt update
sudo apt -y install postgresql

# Postgre SQL Config
wget https://gitlab.com/luckasbosch/archi-azure/-/raw/main/scripts/bdd.sql -P /tmp
sudo -H -u postgres psql -c "create database db_movies"
sudo -H -u postgres psql -U postgres -d db_movies < /tmp/bdd.sql
