#!/bin/bash
# Description :
# 	Update + Docker install & config

# Update
sudo apt update
sudo apt -y upgrade

# Docker Install
sudo apt install -y docker-compose
git clone https://github.com/0xGuillaume/FuzzyApi.git /tmp/FuzzyApi
nohup sudo docker-compose -f /tmp/FuzzyApi/docker-compose.yml up &

