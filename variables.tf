variable "prefix" {
  type    = string
  default = "TPA_"
}


# Virtual Machines Hostnames
variable "vm01" {
	type		= string
	default		= "AZVM01HTML"
	description = "VM01: Static HTML 1"
}

variable "vm02" {
	type		= string
	default		= "AZVM02HTML"
	description = "VM02: Static HTML 2"
}

variable "vm03" {
	type		= string
	default		= "AZVM03WEBA"
	description = "VM03: Docker Application Web + BDD"
}

variable "vm04" {
	type		= string
	default		= "AZVM04PSQL"
	description = "VM04: Base de données Postgre SQL"
}

variable "vm05" {
	type		= string
	default		= "AZVM05USER"
	description = "VM05: Windows User"
}


# Credentials
variable "user" {
	type	= string
	default = "toor"
}

variable "password" {
	type	= string
	default = "Password1234!"
}

# Provisionning
variable "vm01_sh" {
	description = "Provisionning script for VM01"
	type		= string
	default 	= "scripts/vm01.sh"
}

variable "vm02_sh" {
	description = "Provisionning script for VM02"
	type		= string
	default 	= "scripts/vm02.sh"
}

variable "vm03_sh" {
	description = "Provisionning script for VM03"
	type		= string
	default 	= "scripts/vm03.sh"
}

variable "vm04_sh" {
	description = "Provisionning script for VM04"
	type		= string
	default 	= "scripts/vm04.sh"
}

variable "vm05_sh" {
	description = "Provisionning script for VM05"
	type		= string
	default 	= "scripts/vm05.sh"
}

