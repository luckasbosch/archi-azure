#!/bin/bash
# Description :
# 	Update + Packages for a static website

# Update
sudo apt -y update
sudo apt -y upgrade

# Nginx conf
sudo apt -y install nginx
sudo rm /var/www/html/*html
sudo wget https://gitlab.com/luckasbosch/archi-azure/-/raw/main/statics/vm01.html -O /var/www/html/index.html
sudo systemctl restart nginx.service

