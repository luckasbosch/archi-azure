##office the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }

  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}
}


# Resource Group
resource "azurerm_resource_group" "main" {
  name     = "RG_TP_ANNUEL"
  location = "francecentral"
}

# Availibilty Set
resource "azurerm_availability_set" "main" {
  name                = "${var.prefix}AvailibilitySet"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}

# Virtual Network
resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}VNET"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}

# Load Balancer
## Public IP Address
resource "azurerm_public_ip" "lb" {
  name                = "PublicIPForLB"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"
}

## Load Balancer 
resource "azurerm_lb" "lb" {
  name                = "${var.prefix}LB"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  frontend_ip_configuration {
    name                 = "${var.prefix}LB_PublicIPAddress"
    public_ip_address_id = azurerm_public_ip.lb.id
  }
}

# Subnet FrontEnd
## Subnet
resource "azurerm_subnet" "frontend" {
  name                 = "${var.prefix}subnet_frontEnd"
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.0.0/24"]
}

## VM01 ~ Public IP
resource "azurerm_public_ip" "frontend_static_1" {
  name                = "${var.vm01}_PublicIP"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Dynamic"
}

## VM01 ~ NIC
resource "azurerm_network_interface" "frontend_static_1" {
  name                = "${var.vm01}_NIC"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.frontend.id
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.0.0.51"
	  public_ip_address_id		      = azurerm_public_ip.frontend_static_1.id
  }
}

## VM01 ~ Script
data "template_file" "frontend_static_1" {
  template = file("${var.vm01_sh}")
}

## VM01 ~ Static Html 
resource "azurerm_virtual_machine" "frontend_static_1" {
  name                  = "${var.vm01}"
  location              = azurerm_resource_group.main.location
  resource_group_name   = azurerm_resource_group.main.name
  network_interface_ids = [azurerm_network_interface.frontend_static_1.id]
  vm_size               = "Standard_B1s"

  delete_os_disk_on_termination = true
  delete_data_disks_on_termination = true

  availability_set_id = azurerm_availability_set.main.id

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "disk_static_1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.vm01}"
    admin_username = "${var.user}"
    admin_password = "${var.password}"
    custom_data    = data.template_file.frontend_static_1.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags = {
    environment = "staging"
  }
}

## VM02 ~ Public IP
resource "azurerm_public_ip" "frontend_static_2" {
  name                = "${var.vm02}_PublicIP"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Dynamic"
}

## VM02 ~ NIC
resource "azurerm_network_interface" "frontend_static_2" {
  name                = "${var.vm02}_NIC"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.frontend.id
  	public_ip_address_id		      = azurerm_public_ip.frontend_static_2.id
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.0.0.52"
  }
}

## VM02 ~ Script
data "template_file" "frontend_static_2" {
  template = file("${var.vm02_sh}")
}

## VM02 ~ Static Html 
resource "azurerm_virtual_machine" "frontend_static_2" {
  name                = "${var.vm02}"
  location              = azurerm_resource_group.main.location
  resource_group_name   = azurerm_resource_group.main.name
  network_interface_ids = [azurerm_network_interface.frontend_static_2.id]
  vm_size               = "Standard_B1s"

  delete_os_disk_on_termination = true
  delete_data_disks_on_termination = true

  availability_set_id = azurerm_availability_set.main.id

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "disk_static_2"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.vm02}"
    admin_username = "${var.user}"
    admin_password = "${var.password}"
    custom_data    = data.template_file.frontend_static_2.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags = {
    environment = "staging"
  }
}

## VM03 ~ Public IP
resource "azurerm_public_ip" "frontend_webapp" {
  name                = "${var.vm03}_PublicIP"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Dynamic"
}

## VM03 ~ NIC
resource "azurerm_network_interface" "frontend_webapp" {
  name                = "${var.vm03}_NIC"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.frontend.id
    private_ip_address_allocation = "Dynamic"
	public_ip_address_id		  = azurerm_public_ip.frontend_webapp.id
  }
}

## VM03 ~ Script
data "template_file" "frontend_webapp" {
  template = file("${var.vm03_sh}")
}

## VM03 ~ Static Html 
resource "azurerm_virtual_machine" "frontend_webapp" {
  name                = "${var.vm03}"
  location              = azurerm_resource_group.main.location
  resource_group_name   = azurerm_resource_group.main.name
  network_interface_ids = [azurerm_network_interface.frontend_webapp.id]
  vm_size               = "Standard_B1s"

  delete_os_disk_on_termination = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.vm03}_Disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.vm03}"
    admin_username = "${var.user}"
    admin_password = "${var.password}"
    custom_data    = data.template_file.frontend_webapp.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags = {
    environment = "staging"
  }
}


# Subnet BackEnd
## Subnet
resource "azurerm_subnet" "backend" {
  name                 = "${var.prefix}subnet_backEnd"
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.1.0/24"]
}

## VM04 ~ Public IP
resource "azurerm_public_ip" "backend_bdd" {
  name                = "${var.vm04}_PublicIP"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Dynamic"
}

## VM04 ~ NIC
resource "azurerm_network_interface" "backend_bdd" {
  name                = "${var.vm04}_NIC"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.backend.id
    private_ip_address_allocation = "Dynamic"
	public_ip_address_id 		  = azurerm_public_ip.backend_bdd.id
  }
}

## VM04 ~ Script
data "template_file" "backend_bdd" {
  template = file("${var.vm04_sh}")
}

## VM04 ~ Database
resource "azurerm_virtual_machine" "backend" {
  name                = "${var.vm04}"
  location              = azurerm_resource_group.main.location
  resource_group_name   = azurerm_resource_group.main.name
  network_interface_ids = [azurerm_network_interface.backend_bdd.id]
  vm_size               = "Standard_B1s"

  delete_os_disk_on_termination = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.vm04}_Disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.vm04}"
    admin_username = "${var.user}"
    admin_password = "${var.password}"
    custom_data    = data.template_file.backend_bdd.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags = {
    environment = "staging"
  }
}

## BDD	~ MySQL


# Subnet Bureautique
## Subnet
resource "azurerm_subnet" "office" {
  name                 = "${var.prefix}subnet_office"
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.2.0/24"]
}

## IP Public
resource "azurerm_public_ip" "office" {
  name                = "${var.vm05}_PublicIP"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

## Network Security Group - NSG
resource "azurerm_network_security_group" "office" {
  name                = "${var.vm05}_NSG"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}

## Network Security Group - Rules
resource "azurerm_network_security_rule" "office_rdp" {
  name                        = "RDP"
  priority                    = 300
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  destination_port_range      = "3389"
  destination_address_prefix  = "*"
  source_port_range			  = "*"
  source_address_prefix		  = "*"
  resource_group_name         = azurerm_resource_group.main.name
  network_security_group_name = azurerm_network_security_group.office.name
}

## VM05 ~ NIC
resource "azurerm_network_interface" "office" {
  name                = "${var.vm05}_NIC"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.office.id
	public_ip_address_id 		  = azurerm_public_ip.office.id
    private_ip_address_allocation = "Dynamic"
  }
}

## VM05 ~ NSG & NIC association
resource "azurerm_network_interface_security_group_association" "office" {
  network_interface_id      = azurerm_network_interface.office.id
  network_security_group_id = azurerm_network_security_group.office.id
}

## VM05 ~ Windows User
resource "azurerm_windows_virtual_machine" "office" {
  name                = "${var.vm05}"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  size                = "Standard_F2"
  admin_username      = "${var.user}"
  admin_password      = "${var.password}"
  network_interface_ids = [
    azurerm_network_interface.office.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
}


